﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    internal class PartnersBuilder
    {
        private Partner _partner = new Partner();

        public PartnersBuilder WithBaseProperties()
        {
            _partner = new Partner()
            {
                Id = Guid.NewGuid(),
                Name = "PartnerName",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            return this;
        }
        public PartnersBuilder WithNumberIssuedPromoCodes(int number)
        {
            _partner.NumberIssuedPromoCodes = number;

            return this;
        }

        public PartnersBuilder WithActiveLimit()
        {
            _partner.NumberIssuedPromoCodes = 50;
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = DateTime.Now.AddMonths(-1),
                    EndDate = DateTime.Now.AddDays(1),
                    Limit = 100
                }
            };

            return this;
        }
        public PartnersBuilder WithoutActiveLimit()
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = DateTime.Now.AddMonths(-1),
                    EndDate = DateTime.Now.AddDays(-1),
                    CancelDate = DateTime.Now.AddDays(-1),
                    Limit = 100
                }
            };

            return this;
        }
        public Partner Create()
        {
            return _partner;
        }
    }
}
